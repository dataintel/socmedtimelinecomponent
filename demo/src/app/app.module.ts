import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
// import { BaseWidgetComponent } from './base-widget/base-widget.component';
import { MaterialModule } from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import 'hammerjs';
import {SocmedTimelineModule} from '../../../index';

@NgModule({
  declarations: [
    AppComponent,
    // BaseWidgetComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
     MaterialModule,
     BrowserAnimationsModule,
     SocmedTimelineModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
