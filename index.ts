import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseWidgetSocmedTimelineComponent } from './src/base-widget.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from '@angular/material';
// import { FormsModule } from '@angular/forms';
export * from './src/base-widget.component';

@NgModule({
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    MaterialModule,
    // FormsModule
  ],
  declarations: [
	BaseWidgetSocmedTimelineComponent
  ],
  exports: [
  	BaseWidgetSocmedTimelineComponent
  ]
})
export class SocmedTimelineModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SocmedTimelineModule,
      providers: []
    };
  }
}
